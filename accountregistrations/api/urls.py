from django.conf.urls import url
from .views import LoginView, UserProfile, SignUpView

urlpatterns = [
    url(r'^login/$', LoginView.as_view(), name="login"),
    url(r'^detail/$', UserProfile.as_view(), name="user-details"),
    url(r'^signup/$', SignUpView.as_view(), name="signup"),
    ]