from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework.exceptions import NotAcceptable
from rest_framework.serializers import ModelSerializer, Serializer

from accountregistrations.models import Mobile
from pandit.models import Pandit
from shop.models import Shopkeeper


class UserSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'password')


class UserDetailsSerilaizer(ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'
        # fields = ('username', 'first_name','last_name', 'email', 'password' )


class MobileSerializer(ModelSerializer):
    class Meta:
        model = Mobile
        fields = '__all__'


class SignUpSerializer(ModelSerializer):
    password1 = serializers.CharField(write_only=True)
    password2 = serializers.CharField(write_only=True)
    signup_as = serializers.CharField(write_only=True)
    email_confirmed = serializers.BooleanField(default=False, write_only=True)
    mob_no = serializers.CharField(write_only=True)

    class Meta:
        model = User
        fields = (
        'username', 'first_name', 'last_name', 'email', 'password1', 'password2', 'signup_as', 'email_confirmed'
        , 'mob_no')


    def create(self, validated_data):
        p1 = validated_data.pop("password1")
        p2 = validated_data.pop("password2")
        mob = validated_data.pop("mob_no")
        email = validated_data.pop("email_confirmed")
        role = validated_data.pop("signup_as")
        if p1 == p2:
            user = User.objects.create(**validated_data)
            # setattr(user, "password", p1)
            print("1",user.password)
            user.set_password(p1)
            user.save()
            print("2",user.password)
        else:
            raise NotAcceptable(detail ="Unmatched passwords", code=403)
        if email == "true":
            Mobile.objects.create(user=user, Mobile_Number=mob, email_confirmed=True)
        else:
            Mobile.objects.create(user=user, Mobile_Number=mob, email_confirmed=False)

        if role == "pandit":
            Pandit.objects.create(user = user, is_pandit = True)
        elif role == "shopkeeper":
            Shopkeeper.objects.create(user = user, is_shopkeeper = True)
        return user
