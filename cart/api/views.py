from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.generics import ListAPIView, ListCreateAPIView
from rest_framework.response import Response

from accountregistrations.api.serializers import UserSerializer,MobileSerializer, UserDetailsSerilaizer, SignUpSerializer
from accountregistrations.models import Mobile


class LoginView(ObtainAuthToken):
    # queryset = User.objects.all()
    # serializer_class = UserSerializer
    def auth_login(self, request, *args, **kwargs):
        serializer = self.serializer_class(data = request.data, context ={
            'request': request
        })
        serializer.is_valid(raise_exception = True)
        user = serializer.validated_data['user']
        token = Token.objects.get_or_create(user = user)
        return Response({
            'token': token
        })

class UserProfile(ListAPIView):
    queryset = User.objects.all()
    # queryset = Mobile.objects.all()

    serializer_class = UserDetailsSerilaizer
    # serializer_class = MobileSerializer

class SignUpView(ListCreateAPIView):
    queryset = User.objects.all()
    serializer_class = SignUpSerializer
