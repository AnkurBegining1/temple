from .models import Picture, Profile, Temples, Darshans, TempleManager
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from notify.signals import notify
from pagedown.widgets import AdminPagedownWidget
from django.db import models
from django.template.loader import render_to_string
from django.contrib.sites.shortcuts import get_current_site
from django.contrib import messages
from django.utils.http import urlsafe_base64_decode
from django.core.exceptions import PermissionDenied


class TempleAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.TextField: {'widget': AdminPagedownWidget },
    }
    list_display = ['temple2','user','file_link']
    # actions = ['custom_delete']
    #
    #
    # def custom_delete(self, request, queryset):
    #
    #     for object in queryset:
    #         print(object)
    #         profiles = User.objects.filter(is_superuser=False)
    #         for prof in profiles:  # -----deleting temple from all profiles and also from their selected -----------------
    #             # print(prof)
    #             for x in prof.profile.Select_Temple:
    #                 # print(x)
    #                 temp = Temples.objects.get(temple2=x)
    #                 if (temp.id == object.id):
    #                     prof.profile.Select_Temple.remove(x)
    #                     # print("d")
    #                     prof.profile.save()
    #         img = Picture.objects.filter(Temple_id=object.id)
    #         for m in profiles:
    #             for y in m.profile.selected:
    #                 for j in img:
    #                     if y == j.id:
    #                         m.profile.selected.remove(y)
    #                         m.profile.save()
    #         object.delete()
    #
    # custom_delete.short_description = "Delete selected items"
    #
    #
    # def get_actions(self, request):
    #     actions = super(TempleAdmin, self).get_actions(request)
    #     del actions['delete_selected']
    #     return actions
    #
    # def has_delete_permission(self, request, obj=None):
    #     return True

    # def has_delete_permission(self, request, obj=None):
    #     if request.POST and request.POST.get('action') == 'delete_selected':
    #         if '1' in request.POST.getlist('_selected_action'):
    #             messages.add_message(request, messages.ERROR, (
    #                 "Widget #1 is protected, please remove it from your selection "
    #                 "and try again."
    #             ))
    #             return False
    #         return True
    #     return obj is None or obj.pk != 1

class PictureAdmin(admin.ModelAdmin):

    def save_model(self, request, obj, form, change):
        u=[]

        if obj.is_dirty():
          dirty_fields = obj.get_dirty_fields()
          #print(dirty_fields)
          for field in dirty_fields:
            if field == 'image':
              user = User.objects.filter(is_superuser=False)
              for x in user:
                  for y in x.profile.selected:
                      if y==obj.id:
                         u.append(x)
              #print("a1", list(u))
              manager = User.objects.get(id=obj.user_id)
              u.append(manager)
              if not u:
                  recipient = user
              else:
                  recipient = u
              #print("a",list(recipient))
              notify.send(sender=self, target=obj, recipient_list=list(recipient), verb="updated")
              for person in recipient:
                  subject = 'Notification of update'
                  verb = "updated"
                  message = render_to_string('darshan/notification_email.html', {
                      'target': obj, 'verb': verb})
                  person.email_user(subject, message)
              obj.save()





admin.site.register(TempleManager)
admin.site.register(Temples, TempleAdmin)
admin.site.register(Darshans)
admin.site.register(Picture, PictureAdmin)
admin.site.register(Profile)

class MyUserAdmin(UserAdmin):
    # override the default sort column
    ordering = ('-date_joined', )
    # if you want the date they joined or other columns displayed in the list,
    # override list_display too
    list_display = ('username', 'email', 'date_joined', 'first_name', 'last_name', 'is_staff')

# finally replace the default UserAdmin with yours
admin.site.unregister(User)
admin.site.register(User, MyUserAdmin)

#class ProfileInline(admin.StackedInline):
 #   model = Profile
  #  can_delete = False
   # verbose_name_plural = 'Profile'
    #fk_name = 'user'

#class CustomerUserAdmin(UserAdmin):
 #   inlines = (ProfileInline, )
  #  list_display = ('username', 'email', 'first_name', 'last_name', 'is_staff', 'get_temple', 'get_Mobile_No')
    # list_select_related = ('profile',)

    #def get_temple(self, instance):
    #   return instance.profile.Temple
    #get_temple.short_description = 'Temple'

    #def get_Mobile_No(self, instance):
    #   return instance.profile.Mobile_No
    #get_Mobile_No.short_description = 'Mobile_No'

    #def get_inline_instances(self, request, obj=None):
     #   if not obj:
      #      return list()
       # return super(CustomerUserAdmin, self).get_inline_instances(request, obj)


#admin.site.unregister(User)
#admin.site.register(Picture)
#admin.site.register(User, CustomerUserAdmin)