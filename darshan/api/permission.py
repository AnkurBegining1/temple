from rest_framework.permissions import BasePermission, SAFE_METHODS
from darshan.models import TempleManager


class IsOwnerOrReadOnly(BasePermission):
    message = 'You must be owner of this temple'

    def has_object_permission(self, request, view, obj):
        print("Object User", obj.user)
        print("Request User", request.user)
        return obj.user == request.user


class IsTempleManager(BasePermission):
    message = 'You must be login as Temple Manager'

    def has_object_permission(self, request, view, obj):
        print("Request ID", request.id)
        print("Object User",obj.user)
        print("Object Id", obj.id)
        return request.user.temple_manager.is_manager

