from rest_framework.fields import CharField
from rest_framework.serializers import (
    ModelSerializer,
    SerializerMethodField,
    HyperlinkedIdentityField
)

from darshan.models import Temples


# Serializer for Temple List
class TempleListSerializer(ModelSerializer):
    user = SerializerMethodField(read_only=True)
    Temple_List_Detail_Url = HyperlinkedIdentityField(
        view_name="api-temple:temple_detail"
    )
    Temple_List_Delete_Url = HyperlinkedIdentityField(
        view_name="api-temple:temple_delete"
    )
    Temple_List_Update_Url = HyperlinkedIdentityField(
        view_name="api-temple:temple_update"
    )
    Display_image = SerializerMethodField()

    class Meta:
        model = Temples
        fields = [
            'id',
            'temple2',
            'user',
            'Display_image',
            'Temple_List_Detail_Url',
            'Temple_List_Delete_Url',
            'Temple_List_Update_Url'
        ]

    # For changing default value of serializer  field
    # get_Field name which we want to change
    def get_user(self, obj):
        return str(obj.user.username)

    def get_Display_image(self, obj):
        try:
            Display_image = obj.Display_image.url
        except:
            Display_image = None
        return Display_image


# Serializer for Temple Detail
class TempleDetailSerializer(ModelSerializer):
    user = SerializerMethodField(read_only=True)
    temple2 = CharField(label="Temple Name")
    Display_image = SerializerMethodField()

    Latest_to_be_updated_image = SerializerMethodField()
    class Meta:
        model = Temples
        fields = '__all__'

    #For changing default value of serializer  field
    #get_Field where we add get_ before the field which we want to use SerializerMethodField()
    def get_user(self, obj):
        return str(obj.user.username)

    # def get_Display_image(self, obj):
    #     try:
    #         Display_image = obj.Display_image.url
    #     except:
    #         Display_image = None
    #     return Display_image

    def get_Latest_to_be_updated_image(self, obj):
        try:
            Latest_to_be_updated_image = obj.Latest_to_be_updated_image.url
        except:
            Latest_to_be_updated_image = None
        return Latest_to_be_updated_image


# Serializer for Temple Detail
class TempleCreateSerializer(ModelSerializer):
    user = SerializerMethodField(read_only=True)
    temple2 = CharField(label="Temple Name")
    # Display_image = SerializerMethodField()
    # Latest_to_be_updated_image = SerializerMethodField()

    class Meta:
        model = Temples
        fields = '__all__'

