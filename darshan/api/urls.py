from django.conf.urls import url
from django.contrib import admin

from .views import (
    TempleListApiView,
    TempleCreateApiView,
    TempleDetailApiView,
    TempleUpdateApiView,
    TempleDeleteApiView,
)

urlpatterns = [
    url(r'^temple_list/$', TempleListApiView.as_view(), name='temple_list'),
    url(r'^temple_list/create/$', TempleCreateApiView.as_view(), name='temple_create'),
    url(r'^temple_list/detail/(?P<pk>\d+)/$', TempleDetailApiView.as_view(), name='temple_detail'),
    url(r'^temple_list/update/(?P<pk>\d+)/$', TempleUpdateApiView.as_view(), name='temple_update'),
    url(r'^temple_list/delete/(?P<pk>\d+)/$', TempleDeleteApiView.as_view(), name='temple_delete'),


]
