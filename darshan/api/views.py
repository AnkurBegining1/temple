from rest_framework.generics import (
    ListAPIView,
    CreateAPIView,
    RetrieveAPIView,
    UpdateAPIView,
    DestroyAPIView,
    RetrieveUpdateAPIView
)
# Use for Permission
from rest_framework.permissions import (
    AllowAny,
    IsAdminUser,
    IsAuthenticated,
    IsAuthenticatedOrReadOnly
)
# For search filter
from rest_framework.filters import (
    SearchFilter,
    OrderingFilter
)

# For serializing the field
from .serializers import (
    TempleListSerializer,
    TempleDetailSerializer,
    TempleCreateSerializer
)

from .pagination import (
    TempleLimitOffsetPagination,
    TemplePageNumberPagination
)

from .permission import (
    IsOwnerOrReadOnly,
    IsTempleManager
)

from darshan.models import Temples


# ListAPIView is used for retriving all the list
class TempleListApiView(ListAPIView):
    serializer_class = TempleListSerializer
    permission_classes = [AllowAny]
    # pagination_class = TemplePageNumberPagination

    # Use rest_framework for searching way
    filter_backends = [SearchFilter, OrderingFilter]
    search_fields = ['temple2', 'user_id__username']
    # To-Do:: Order by Temple_id
    queryset = Temples.objects.all().order_by()


class TempleCreateApiView(CreateAPIView):
    permission_classes = [IsTempleManager, IsAuthenticated]
    serializer_class = TempleCreateSerializer
    queryset = Temples.objects.all()


class TempleDetailApiView(RetrieveAPIView):
    permission_classes = [AllowAny]
    serializer_class = TempleDetailSerializer
    queryset = Temples.objects.all()


class TempleUpdateApiView(RetrieveUpdateAPIView):
    permission_classes = [IsOwnerOrReadOnly, IsAuthenticatedOrReadOnly]
    serializer_class = TempleDetailSerializer
    queryset = Temples.objects.all()


class TempleDeleteApiView(DestroyAPIView):
    print("Coming to Temple Delete API View")
    permission_classes = [IsOwnerOrReadOnly, IsAuthenticatedOrReadOnly]
    serializer_class = TempleDetailApiView
    queryset = Temples.objects.all()
