from django.http import Http404
from rest_framework.permissions import BasePermission, SAFE_METHODS
from darshan.models import TempleManager

#
# class IsOwnerOrReadOnly(BasePermission):
#     message = 'You must be owner of this temple'
#
#     def has_object_permission(self, request, view, obj):
#         print("Object User", obj.user)
#         print("Request User", request.user)
#         return obj.user == request.user
#
#
# class IsAllowed(BasePermission):
#     message = 'You must be login as Temple Manager'
#
#     def has_object_permission(self, request, view, obj):
#         # Read permissions are allowed to any request,
#         # so we'll always allow GET, HEAD or OPTIONS requests.
#         if request.method in SAFE_METHODS:
#             return True
#         print(request.user.temple_manager.is_manager or request.user.shopkeeper.is_shopkeeper)
#
#         # Write permissions are only allowed to the owner of the snippet.
#         return request.user.temple_manager.is_manager or request.user.shopkeeper.is_shopkeeper
#