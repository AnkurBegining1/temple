from rest_framework import serializers
from  rest_framework.serializers import ModelSerializer
from shop.models import Product

class ProductSerializers(ModelSerializer):
    # photo1 = serializers.ImageField(upl)
    class Meta:
        model = Product
        fields = '__all__'

    def create(self, validated_data):
        print(validated_data)
        obj = Product.objects.create(**validated_data)
        return obj