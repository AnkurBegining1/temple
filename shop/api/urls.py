from django.conf.urls import url, include

from .views import ProductView, ProductDetailView, ProductRemoveView

urlpatterns = [
    url(r'^products/$', ProductView.as_view(), name='product-list'),
    url(r'^product/(?P<pk>[0-9]+)/$', ProductDetailView.as_view(), name='product-detail'),
    url(r'^product_remove/(?P<pk>[0-9]+)/$', ProductRemoveView.as_view(), name='product-remove'),

]

