from django.http import HttpResponse, Http404
from rest_framework import status
from rest_framework.generics import ListCreateAPIView,DestroyAPIView, RetrieveUpdateAPIView
from rest_framework.permissions import IsAuthenticatedOrReadOnly

# from shop.api.permission import IsOwnerOrReadOnly, IsAllowed
from rest_framework.response import Response

from .serializers import ProductSerializers
from shop.models import Product
from django.views.decorators.csrf import csrf_exempt

class ProductView(ListCreateAPIView):
    queryset = Product.objects.all()
    permission_classes =  [ IsAuthenticatedOrReadOnly]
    serializer_class = ProductSerializers

    def perform_create(self, serializer):
        # print(IsAllowed, IsAuthenticatedOrReadOnly, self.request.user)
        try:
            if(self.request.user.temple_manager.is_manager or self.request.user.shopkeeper.is_shopkeeper):
                serializer.save()
            else:

                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        except:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ProductDetailView(RetrieveUpdateAPIView):
    permission_classes = [ IsAuthenticatedOrReadOnly]
    serializer_class = ProductSerializers
    queryset = Product.objects.all()

class ProductRemoveView(DestroyAPIView):
    permission_classes = [IsAuthenticatedOrReadOnly]
    serializer_class = ProductSerializers
    queryset = Product.objects.all()